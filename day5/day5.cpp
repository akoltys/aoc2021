#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <list>
#include <vector>
#include <algorithm>
#include <numeric>

using namespace std;

int main() {
    cout << "AoC2021 Day 5\n";
    ifstream infile("input");
    string line;
    vector<vector<int>> ventsList(1000, vector<int>(1000, 0));
    int overlapingLines = 0;
    int overlapingLines2 = 0;
    while(getline(infile, line)) {
        replace(line.begin(), line.end(), ',', ' ');
        cout << "Input: " << line << '\n';
        int x1,y1,x2,y2;
        string tmp;
        stringstream ss(line);
        ss >> x1 >> y1 >> tmp >> x2 >> y2;
        if (x1 != x2 && y1 != y2) {
            continue;
        }
        cout << "X1: " << x1 << " Y1: " << y1 << " X2: " << x2 << " Y2: " << y2 << '\n';
        for (auto x=min(x1, x2); x <= max(x1, x2); x++) {
            for (auto y=min(y1, y2); y <= max(y1, y2); y++) {
                //cout << "Marking: " << x << ',' << y << " position\n";
                ventsList[x][y] +=1;
                if (ventsList[x][y] == 2) {
                    overlapingLines++;
                }
            }
        }
    }
    infile.close();
    cout << "Part 1 Result: " << overlapingLines << '\n';
    ifstream infile2("input");
    while(getline(infile2, line)) {
        replace(line.begin(), line.end(), ',', ' ');
        cout << "Input: " << line << '\n';
        int x1,y1,x2,y2;
        string tmp;
        stringstream ss(line);
        ss >> x1 >> y1 >> tmp >> x2 >> y2;
        cout << "X1: " << x1 << " Y1: " << y1 << " X2: " << x2 << " Y2: " << y2 << '\n';
        if (x1 == x2 || y1 == y2) {
            continue;
        }
        cout << "X1: " << x1 << " Y1: " << y1 << " X2: " << x2 << " Y2: " << y2 << '\n';
        int y = x1 < x2 ? y1 : y2;
        int delta = 0;
        if ((y == y1 && y1 < y2) || (y==y2 && y2 < y1)) {
            delta = 1;
        } else {
            delta = -1;
        }
        for (auto x=min(x1, x2); x <= max(x1, x2); x++) {
            cout << "Marking: " << x << ',' << y << " position\n";
            ventsList[x][y] +=1;
            if (ventsList[x][y] == 2) {
                overlapingLines++;
            }
            y += delta;
        }
    }
    for (auto y = 0; y < 10; y++) {
        for (auto x = 0; x < 10; x++) {
            if (ventsList[x][y] != 0) {
                cout << ventsList[x][y];
            } else {
                cout << '.';
            }
        }
        cout << '\n';
    }
    cout << "Part 2 Result: " << overlapingLines << '\n';
    return 0;
}

