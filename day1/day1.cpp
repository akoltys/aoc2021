#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <list>

using namespace std;

int main() {
    cout << "AoC2021 Day 1\n";
    ifstream infile("input");
    string line;
    int prev = -1;
    int cur = 0;
    int inccnt = 0;
    while(getline(infile, line)) {
        istringstream iss(line);
        iss >> cur;
        if (prev != -1 && cur > prev) {
            inccnt++;
        }
        prev = cur;
    }
    cout << "PART1: Inc cnt: " << inccnt << '\n';
    infile.close();
    ifstream infile2("input");
    list<int> wdw;
    prev = -1;
    cur = 0;
    inccnt = 0;
    while(getline(infile2, line)) {
        istringstream iss(line);
        int tmp;
        iss >> tmp;
        cur += tmp;
        wdw.push_back(tmp);
        if (wdw.size() >= 3) {
            cout << "Prev: " << prev << " Cur: " << cur << '\n';
            if (prev != -1 && cur > prev) {
                inccnt++;
            }
            prev = cur;
            cur -= wdw.front();
            wdw.pop_front();
        }
    }
    cout << "PART2: Inc cnt: " << inccnt << '\n';
    return 0;
}
