#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <list>
#include <vector>
#include <algorithm>
#include <numeric>
#include <cstdint>
#include <map>

using namespace std;

struct fish_o {
    int val;
    int64_t day;
};

map<int, int64_t> lookup;

int64_t countFishFamily(const fish_o& fish, int days) {
    if (fish.day == 0 && lookup.find(fish.val) != lookup.end()) {
        return lookup[fish.val];
    } 
    int64_t cnt = 1;
    int tmpday = fish.val+1+fish.day;
    while (tmpday <= days) {
        cnt += countFishFamily({8, tmpday}, days);
        tmpday += 7;
    }
    return cnt;
}

int main() {
    cout << "AoC2021 Day 6\n";
    ifstream infile("input");
    string line;
    int64_t fishCnt = 0;
    getline(infile, line);
    replace(line.begin(), line.end(), ',', ' ');
    cout << "Input: " << line << '\n';
    stringstream ss(line);
    vector<fish_o> fishes; 
    while (!ss.eof()) {
        int tmp;
        ss >> tmp;
        fishes.push_back({tmp, 0});
        cout << " Val: " << tmp;
    }
    cout << '\n';
    const int days = 256;
    for (int idx = 0; idx < fishes.size(); idx++) {
        cout << "Checking at idx " << idx+1 << "/" << fishes.size() << " val: " << fishes[idx].val << ' ';
        int64_t tmp = countFishFamily(fishes[idx], days);
        cout << " got: " << tmp << '\n';
        lookup[fishes[idx].val] = tmp;
        cout << "\tLookup at: " << fishes[idx].val << " : " << lookup[fishes[idx].val] << '\n';
        fishCnt += tmp;
//      initial version, not memory optimized
//        auto& fish = fishes[idx];
//        if (fish.day > days) {
//            continue;
//        }
//        int tmp = fish.val;
//        int newTmp = (tmp-(days-fish.day));
//        newTmp = (newTmp < 0) ? newTmp % 7 : newTmp;
//        newTmp = (newTmp < 0) ? newTmp + 7 : newTmp;
//        fish.val = newTmp;
//        int tmpday = tmp+1+fish.day;
//        while (tmpday <= days) {
//            fishes.push_back({8, tmpday});
//            tmpday += 7;
//        }
    }
    cout << '\n';
    cout << "Part 1 Result: " << fishCnt << '\n';
    return 0;
}

