#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <list>
#include <vector>
#include <algorithm>
#include <numeric>
#include <cstdint>
#include <map>
#include <set>
#include <utility>

using namespace std;

struct caveNode {
    string val;
    set<string> nbrs;
    bool big;
    caveNode(string name, string nbr) : val(name), nbrs({nbr}) {
        if (name[0] >= 'A' && name[0] <= 'Z') {
            big = true;
        } else {
            big = false;
        }
    }

    void print() {
        cout << (big ? "Big" : "Small") << " cave name: " << val << " nbrs:\n";
        for (auto nbr: nbrs) {
            cout << '\t' << nbr << '\n';
        }
        cout << '\n';
    }
};

int getPathCnt(string start, string stop, const map<string, caveNode>& caves, set<string> visited) {
    if (start == stop) {
        return 1;
    }
    int pathCnt = 0;
    auto toVisit = caves.at(start).nbrs;
    if (!caves.at(start).big) {
        visited.insert(start);
    }
    for (auto next: toVisit) {
        if (visited.find(next) == visited.end()) {
            pathCnt += getPathCnt(next, stop, caves, visited);
        }
    }
    return pathCnt;
}

set<string> getPathCnt2(string start, string stop, const map<string, caveNode>& caves, 
                        set<string> visited, string scave, int scnt, 
                        string path, set<string>& paths,
                        set<string>& lut) {
    if (start == stop) {
        if (paths.find(path) == paths.end()) {
            cout << paths.size()+1 << " " << path << '\n';
            paths.insert(path);
        }
        return paths;
    }
//    if (scnt == 0) {
//        if (lut.find(start) != lut.end()) {
//            return paths;
//        }
//    }
    int pathCnt = 0;
    auto toVisit = caves.at(start).nbrs;
    if (scave != start && !caves.at(start).big) {
        visited.insert(start);
    } else if (scave == start) {
        if (scnt > 0) {
            scnt--;
        } else {
            visited.insert(start);
        }
    }
    for (auto next: toVisit) {
        if (visited.find(next) == visited.end()) {
            getPathCnt2(next, stop, caves, visited, scave, scnt, path+","+next, paths, lut);
        }
    }
//    if (scnt == 0) {
//        lut.insert(start);
//    }
    return paths;
}

int main() {
    cout << "AoC2021 Day 12\n";
    ifstream infile("input");
    string line;
    int pathsCnt = 0;
    map<string, caveNode> caves;
    while(getline(infile, line)) {
        auto pos = line.find('-');
        string tmpCave = line.substr(0, pos);
        string tmpNbr = line.substr(pos+1);
        cout << "Node: " << tmpCave << " Nbr: " << tmpNbr << '\n';
        if (caves.find(tmpCave) == caves.end()) {
            caveNode tmpNode(tmpCave, tmpNbr);
            caves.insert(pair<string, caveNode>(tmpCave, tmpNode));
            if (caves.find(tmpNbr) == caves.end()) {
                caveNode tmpNode(tmpNbr, tmpCave);
                caves.insert(pair<string, caveNode>(tmpNbr, tmpNode));
            } else {
                caves.at(tmpNbr).nbrs.insert(tmpCave);
            }
        } else {
            caves.at(tmpCave).nbrs.insert(tmpNbr);
            if (caves.find(tmpNbr) == caves.end()) {
                caveNode tmpNode(tmpNbr, tmpCave);
                caves.insert(pair<string, caveNode>(tmpNbr, tmpNode));
            } else {
                caves.at(tmpNbr).nbrs.insert(tmpCave);
            }
        }
    }
    for (auto cave: caves) {
        cave.second.print();
    }
    pathsCnt = getPathCnt("start", "end", caves, {});
    cout << "Part 1 Result: " << pathsCnt << '\n';
    int pathsCnt2 = 0;
    set<string> tmpPaths;
    set<string> tmpLut;
    for (auto cave: caves) {
        if (cave.second.big == false && cave.second.val != "start" && cave.second.val != "end") {
            cout << "Checking for: " << cave.second.val << " Current: " << tmpPaths.size() << '\n';
            set<string> pathList = getPathCnt2("start", 
                                        "end", 
                                        caves, 
                                        {}, 
                                        cave.second.val, 
                                        1, 
                                        string("start"), 
                                        tmpPaths,
                                        tmpLut);
        }
    }
    pathsCnt2 = tmpPaths.size();
    cout << "Part 2 Result: " << pathsCnt2 << '\n';
    return 0;
}

