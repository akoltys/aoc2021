#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <list>
#include <vector>
#include <algorithm>
#include <numeric>
#include <cstdint>
#include <map>
#include <set>
#include <utility>

using namespace std;


// ): 3 points.
// ]: 57 points.
// }: 1197 points.
// >: 25137 points.
map<char, int> gScores {
    {')', 3},
    {']', 57},
    {'}', 1197},
    {'>', 25137}
};

map<char, int> gScores2 {
    {')', 1},
    {']', 2},
    {'}', 3},
    {'>', 4}
};


bool isStartChar(char c) {
    return c == '(' || c == '[' || c == '{' || c == '<';
}

bool isValidEndChar(char c, const vector<char>& charStack) {
    if (c == ')' && charStack.back() == '(') {
        return true;
    }
    if (c == ']' && charStack.back() == '[') {
        return true;
    }
    if (c == '}' && charStack.back() == '{') {
        return true;
    }
    if (c == '>' && charStack.back() == '<') {
        return true;
    }
    return false;
}

int main() {
    cout << "AoC2021 Day 9\n";
    ifstream infile("input");
    string line;
    int illegalScore = 0;
    vector<string> inputLines;
    vector<vector<char>> incompleteLines;
    while(getline(infile, line)) {
        inputLines.push_back(line);
    }
    cout << "Found " << inputLines.size() << " lines to parse.\n";
    for (auto lineToParse: inputLines) {
        cout << "Paring line: " << lineToParse << '\n';
        vector<char> charStack;
        bool syntaxError = false;
        for (auto c: lineToParse) {
            if (isStartChar(c)) {
                charStack.push_back(c);
            } else {
                if (isValidEndChar(c, charStack)) {
                    charStack.pop_back();
                } else {
                    cout << "Found illegal char: " << c << '\n';
                    illegalScore += gScores[c];
                    syntaxError = true;
                    break;
                }
            }
        }
        if (!syntaxError) {
            incompleteLines.push_back(charStack);
        }
    }

    cout << "Part 1 Result: " << illegalScore << '\n';
    cout << "Lines which are incomplete cnt: " << incompleteLines.size() << "\n";
    vector<int64_t> autoScores;
    for (auto lineToComplete: incompleteLines) {
        int64_t tmpScore = 0;
        while (lineToComplete.size() > 0) {
            auto tmp = lineToComplete.back();
            lineToComplete.pop_back();
            char closingChar;
            if (tmp == '(') {
                closingChar = ')';
            } else if (tmp == '[') {
                closingChar = ']';
            } else if (tmp == '{') {
                closingChar = '}'; 
            } else {
                closingChar = '>';
            }
            tmpScore = tmpScore*5 + gScores2[closingChar];
        }
        autoScores.push_back(tmpScore);
    }
    sort(autoScores.begin(), autoScores.end());
    for (int idx = 0; idx < autoScores.size(); idx++) {
        cout << "Idx. " << idx+1 << " score: " << autoScores[idx] << '\n';
    }
    auto middleIdx = (autoScores.size()+1)/2 - 1;
    cout << "Choosing score at idx: " << middleIdx << '\n';
    auto middleScore = autoScores[middleIdx];
    cout << "Part 2 Result: " << middleScore << '\n';
    return 0;
}

