#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <list>

using namespace std;

int main() {
    cout << "AoC2021 Day 2\n";
    ifstream infile("input");
    string line;
    int pos_h = 0;
    int pos_v = 0;
    while(getline(infile, line)) {
        istringstream iss(line);
        string dir;
        int val;
        iss >> dir >> val;
        if (dir == "forward") {
            pos_h += val;
        } else if (dir == "up") {
            pos_v -= val;
        } else {
            pos_v += val;
        }
    }
    cout << "Part 1: pos_h: " << pos_h << " pos_v: " << pos_v << " vol: " << pos_h*pos_v << '\n';
    infile.close();
    ifstream infile2("input");
    pos_h = 0;
    pos_v = 0;
    int aim = 0;
    while(getline(infile2, line)) {
        istringstream iss(line);
        string dir;
        int val;
        iss >> dir >> val;
        if (dir == "forward") {
            pos_h += val;
            pos_v += aim*val;
        } else if (dir == "up") {
            aim -= val;
        } else {
            aim += val;
        }
    }
    cout << "Part 1: pos_h: " << pos_h << " pos_v: " << pos_v << " vol: " << pos_h*pos_v << '\n';
    return 0;
}
