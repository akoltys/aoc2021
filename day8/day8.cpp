#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <list>
#include <vector>
#include <algorithm>
#include <numeric>
#include <cstdint>
#include <map>

using namespace std;

map<string, int> segSeqence = {
    {"abcefg", 0},
    {"cf", 1},
    {"acdeg", 2},
    {"acdfg", 3},
    {"bcdf", 4},
    {"abdfg", 5},
    {"abdefg", 6},
    {"acf", 7},
    {"abcdefg", 8},
    {"abcdfg", 9}
};

map<int, int> easyDigitSegCnt = {
    // number of segments, digit
    {2, 1},
    {4, 4},
    {3, 7},
    {7, 8}
};

bool isMapValid(const map<char, char>& segMap, const vector<string>& numbers) {
    for (auto number: numbers) {
        for(auto idx = 0; idx < number.size(); idx++) {
            number[idx] = segMap.at(number[idx]);
        }
        sort(number.begin(), number.end());
        if (segSeqence.find(number) == segSeqence.end()) {
            return false;
        }
    }
    return true;
}

map<char, char> findBestMap(string segs, char curSeg, map<char, char> segMap, const vector<string>& numbers) {
    if (segs.size() == 0) {
        // check current map
        if (isMapValid(segMap, numbers)) {
            return segMap;
        }
        return {};
    }

    for (auto seg: segs) {
        segMap[curSeg] = seg;
        auto newSegs = segs;
        newSegs.erase(std::remove(newSegs.begin(), newSegs.end(), seg), newSegs.end());
        auto result = findBestMap(newSegs, curSeg+1, segMap, numbers);
        if (!result.empty()) {
            return result;
        }
    }
    return {};
}

int main() {
    cout << "AoC2021 Day 8\n";
    ifstream infile("input");
    string line;
    vector<string> fLines;
    int easyValsCnt = 0;
    while(getline(infile, line)) {
        fLines.push_back(line);
    }
    // Part 1
    for (auto input: fLines) {
        auto pos = input.find("| ");
        auto data = input.substr(pos+2);
        cout << "Checking: " << data << '\n';
        stringstream ss(data);
        while(!ss.eof()) {
            string tmp;
            ss >> tmp;
            auto segCnt = tmp.size();
            if (easyDigitSegCnt.find(segCnt) != easyDigitSegCnt.end()) {
                easyValsCnt++;
            }
        }
    }
    cout << "Part 1 Result: " << easyValsCnt << '\n';
    int outputSum = 0;
    for (auto input: fLines) {
        auto pos = input.find("| ");
        auto output = input.substr(pos+2);
        replace(input.begin(), input.end(), '|', ' ');
        vector<string> numbers;
        stringstream ss(input);
        while(!ss.eof()) {
            string tmp;
            ss >> tmp;
            sort(tmp.begin(), tmp.end());
            cout << tmp << ' ';
            numbers.push_back(tmp);
        }
        cout << '\n';
        auto segMap = findBestMap("abcdefg", 'a', {}, numbers);
        if (segMap.empty()) {
            cout << "Failed to find seg mapping\n";
        } else {
            cout << input << '\n';
            int multiplier = 1000;
            for (auto offset = numbers.size()-4; offset < numbers.size(); offset++) {
                auto tmpNumber = numbers[offset];
                for(auto idx = 0; idx < tmpNumber.size(); idx++) {
                    tmpNumber[idx] = segMap.at(tmpNumber[idx]);
                }
                sort(tmpNumber.begin(), tmpNumber.end());
                cout << segSeqence[tmpNumber];
                outputSum += segSeqence[tmpNumber]*multiplier;
                multiplier /= 10;
            }
            cout << '\n';
        }
    }
    cout << "Part 2 Result: " << outputSum << '\n';
    return 0;
}

