#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <list>
#include <vector>
#include <algorithm>
#include <numeric>
#include <cstdint>
#include <map>
#include <set>
#include <utility>

using namespace std;

using xy_point = pair<int, int>;

bool isValidPoint(const xy_point point, int maxRow, int maxCol) {
    if (point.first < 0 || point.first >= maxRow) {
        return false;
    }
    if (point.second < 0 || point.second >= maxCol) {
        return false;
    }
    return true;
}

int getBasinSize(xy_point basin, const vector<vector<int>>& heightMap) {
    int maxRow = heightMap.size();
    int maxCol = heightMap[0].size();
    set<xy_point> visited;
    vector<xy_point> toVisit = {basin};
    while (toVisit.size() > 0) {
        auto cur = toVisit.back();
        toVisit.pop_back();
        visited.insert(cur);
        vector<xy_point> points = { { cur.first-1, cur.second },
                                    { cur.first+1, cur.second },
                                    { cur.first, cur.second-1 },
                                    { cur.first, cur.second+1 } };
        for (auto point: points) {
            if (isValidPoint(point, maxRow, maxCol) && visited.find(point) == visited.end()) {
                if (heightMap[point.first][point.second] != 9 &&
                    heightMap[point.first][point.second] > heightMap[cur.first][cur.second]) {
                    toVisit.push_back(point);
                }
            }
        }
    }
    return visited.size();
}

int main() {
    cout << "AoC2021 Day 9\n";
    ifstream infile("input");
    string line;
    int sumOfRiskLevels = 0;
    vector<vector<int>> heightmap;
    vector<xy_point> basins;
    while(getline(infile, line)) {
        cout << line << '\n';
        vector<int> tmp;
        for (auto c: line) {
            tmp.push_back(c-'0');
        }
        heightmap.push_back(tmp);
    }

    // debug check parsing input
    //for (auto row: heightmap) {
    //    for (auto h: row) {
    //        cout << h << ' ';
    //    }
    //    cout << '\n';
    //}

    for (auto row = 0; row < heightmap.size(); row++) {
        for (auto col = 0; col < heightmap[0].size(); col++) {
            auto up = row - 1;
            auto down = row + 1;
            auto left = col - 1;
            auto right = col + 1;
            if (up >= 0 && !(heightmap[row][col] < heightmap[up][col])) {
                continue;
            }
            if (down < heightmap.size() && !(heightmap[row][col] < heightmap[down][col])) {
                continue;
            }
            if (left >= 0 && !(heightmap[row][col] < heightmap[row][left])) {
                continue;
            }
            if (right < heightmap[0].size() && !(heightmap[row][col] < heightmap[row][right])) {
                continue;
            }
            // cout << "map[" << row << "][" << col << "] = " << heightmap[row][col] << '\n';
            sumOfRiskLevels += heightmap[row][col] + 1;
            basins.push_back({row, col});
        }
    }

    cout << "Part 1 Result: " << sumOfRiskLevels << '\n';
    cout << "Basins to check: " << basins.size() << '\n';
    vector<int> basinSizes;
    for (auto basin: basins) {
        auto tmp = getBasinSize(basin, heightmap);
        basinSizes.push_back(tmp);
    }
    sort(basinSizes.begin(), basinSizes.end());
    auto basinCnt = basinSizes.size();
    int64_t largestBasinsMultiplied = basinSizes[basinCnt-1]*basinSizes[basinCnt-2]*basinSizes[basinCnt-3];
    cout << "Part 2 Result: " << largestBasinsMultiplied << '\n';
    return 0;
}

