#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <list>
#include <vector>
#include <algorithm>
#include <numeric>
#include <cstdint>
#include <map>
#include <set>
#include <utility>
#include <limits>

using namespace std;

string hexToBinString(const string& s) {
    const map<char, const string> binVals = {
        {'0', "0000"},
        {'1', "0001"},
        {'2', "0010"},
        {'3', "0011"},
        {'4', "0100"},
        {'5', "0101"},
        {'6', "0110"},
        {'7', "0111"},
        {'8', "1000"},
        {'9', "1001"},
        {'A', "1010"},
        {'B', "1011"},
        {'C', "1100"},
        {'D', "1101"},
        {'E', "1110"},
        {'F', "1111"},
    };
    string result;
    for (auto c: s) {
        auto bin = binVals.at(c);
        result.append(bin);
    }
    return result;
}

enum PacketType {
    Sum = 0,
    Product = 1,
    Min = 2,
    Max = 3,
    Literal = 4,
    Gt = 5,
    Lt = 6,
    Eq = 7,
    Other,
};

struct PacketGeneric {
    int version;
    int type;
    PacketGeneric(int ver, int type) : version(ver), type(type) {}
};

struct PacketLiteral : public PacketGeneric {
    int64_t number;
    PacketLiteral(int ver, int type) : PacketGeneric(ver, type) {}
};

struct PacketOperator : public PacketGeneric {
    int lengthId;
    union {
        int subPacketLen;
        int subPacketCnt;
    };
    vector<PacketGeneric *> subPackets;
    PacketOperator(int ver, int type) : PacketGeneric(ver, type) {}
};

PacketGeneric* parseInput(const string& bin, int& offset) {
    int cOffset = offset;
    int version = stoi(bin.substr(cOffset, 3), nullptr, 2);
    cOffset += 3;
    int type = stoi(bin.substr(cOffset, 3), nullptr, 2);
    cOffset += 3;
    //cout << "Packet Version: " << version << '\n';
    //cout << "Packet Type:    " << type << '\n';
    PacketGeneric *result = nullptr;
    if (type == PacketType::Literal) {
        // Parsing Literal value
        PacketLiteral *rLiteral = new PacketLiteral(version, type);
        result = rLiteral;
        int64_t tmpNbr = 0;
        bool prev = true;
        while(prev) {
            prev = bin[cOffset] == '1';
            //cout << "Parsing chunk: " << bin.substr(cOffset, 5) << " Prev: " << bin[cOffset] << '\n';
            cOffset += 1;
            int tmp = stoi(bin.substr(cOffset, 4), nullptr, 2);
            cOffset += 4;
            tmpNbr = tmpNbr << 4 | tmp;
        }
        rLiteral->number = tmpNbr;
        //cout << "Literal number: " << tmpNbr << '\n';
    } else {
        // Parsing Operator value
        PacketOperator *rOperator = new PacketOperator(version, type);
        int tmpLenId = bin[cOffset] == '1';
        cOffset += 1;
        rOperator->lengthId = tmpLenId;
        if (tmpLenId) {
            int tmpCnt = stoi(bin.substr(cOffset, 11), nullptr, 2); 
            rOperator->subPacketCnt = tmpCnt;
            cOffset += 11;
            //cout << "Sub packets count: " << rOperator->subPacketCnt << '\n';
            while (tmpCnt > 0) {
                auto tmpSub = parseInput(bin, cOffset);
                rOperator->subPackets.push_back(tmpSub);
                tmpCnt--;
            }
        } else {
            int tmpLen = stoi(bin.substr(cOffset, 15), nullptr, 2); 
            rOperator->subPacketLen = tmpLen;
            cOffset += 15;
            //cout << "Sub packets length: " << rOperator->subPacketLen << '\n';
            int subsEnd = cOffset + tmpLen;
            while (cOffset < subsEnd) {
                auto tmpSub = parseInput(bin, cOffset);
                rOperator->subPackets.push_back(tmpSub);
            }
        }
        result = rOperator;
    }
    offset = cOffset;
    return result;    
}

int sumVersions(PacketGeneric* pkt) {
    int result = pkt->version;
    if (pkt->type != PacketType::Literal) {
        PacketOperator *oPkt = static_cast<PacketOperator *>(pkt);
        for (auto s: oPkt->subPackets) {
            result += sumVersions(s);
        }
    }
    return result;
}

int64_t evaluatePackets(PacketGeneric* pkt) {
    int64_t result = 0;
    switch (pkt->type) {
        case PacketType::Sum: 
            {
                PacketOperator *oPkt = static_cast<PacketOperator *>(pkt);
                //cout << "Sum of: " << oPkt->subPackets.size() << "{\n";
                for (auto s: oPkt->subPackets) {
                    result += evaluatePackets(s);
                }
            }
            break;
        case PacketType::Product:
            {
                PacketOperator *oPkt = static_cast<PacketOperator *>(pkt);
                //cout << "Product:  of: " << oPkt->subPackets.size() << "{\n";
                result = 1;
                for (auto s: oPkt->subPackets) {
                    result *= evaluatePackets(s);
                }
            }
            break;
        case PacketType::Min:
            {
                PacketOperator *oPkt = static_cast<PacketOperator *>(pkt);
                //cout << "Min:  of: " << oPkt->subPackets.size() << "{\n";
                result = numeric_limits<int64_t>::max();
                for (auto s: oPkt->subPackets) {
                    auto tmp = evaluatePackets(s);
                    if (tmp < result) {
                        result = tmp;
                    }
                }
            }
            break;
        case PacketType::Max:
            {
                PacketOperator *oPkt = static_cast<PacketOperator *>(pkt);
                //cout << "Max:  of: " << oPkt->subPackets.size() << "{\n";
                result = numeric_limits<int64_t>::min();
                for (auto s: oPkt->subPackets) {
                    auto tmp = evaluatePackets(s);
                    if (tmp > result) {
                        result = tmp;
                    }
                }
            }
            break;
        case PacketType::Literal:
            {
                //cout << "Literal: {\n";
                PacketLiteral *oPkt = static_cast<PacketLiteral *>(pkt);
                result = oPkt->number;
            }
            break;
        case PacketType::Gt:
            {
                PacketOperator *oPkt = static_cast<PacketOperator *>(pkt);
                //cout << "Greater of: " << oPkt->subPackets.size() << "{\n";
                result = evaluatePackets(oPkt->subPackets[0]) > evaluatePackets(oPkt->subPackets[1]);
            }
            break;
        case PacketType::Lt:
            {
                PacketOperator *oPkt = static_cast<PacketOperator *>(pkt);
                //cout << "Less of: " << oPkt->subPackets.size() << "{\n";
                result = evaluatePackets(oPkt->subPackets[0]) < evaluatePackets(oPkt->subPackets[1]);
            }
            break;
        case PacketType::Eq:
            {
                PacketOperator *oPkt = static_cast<PacketOperator *>(pkt);
                //cout << "Equal of: " << oPkt->subPackets.size() << "{\n";
                result = evaluatePackets(oPkt->subPackets[0]) == evaluatePackets(oPkt->subPackets[1]);
            }
            break;
        default:
            break;
    }
    //cout << ' ' << result << " }\n"; 
    return result;
}

int main() {
    cout << "AoC2021 Day 16\n";
    ifstream infile("input");
    string line;
    int verionSum = 0;
    string inputHex;
    int tmpOffset = 0;
    while(getline(infile, line)) {
        inputHex = line;
    }
    PacketGeneric *pkt = nullptr;
    auto inputBin = hexToBinString(inputHex);
    cout << "Input HEX: " << inputHex << '\n';
    cout << "Input BIN: " << inputBin << '\n';
    pkt = parseInput(inputBin, tmpOffset);
    cout << "Packet version sum: " << sumVersions(pkt) << '\n';
    cout << "Evaluation result: " << evaluatePackets(pkt) << '\n';
    
    // testing
    // tmpOffset = 0;
    // cout << "\n\nInput BIN: 110100101111111000101000\n";
    // pkt = parseInput("110100101111111000101000", tmpOffset);
    // cout << "Packet version sum: " << sumVersions(pkt) << '\n';
    // cout << "Part 1 Result: " << verionSum << '\n';

    // tmpOffset = 0;
    // inputBin = hexToBinString("9C0141080250320F1802104A08");
    // pkt = parseInput(inputBin, tmpOffset);
    // cout << "Evaluation result: " << evaluatePackets(pkt) << '\n';
    return 0; 
}

