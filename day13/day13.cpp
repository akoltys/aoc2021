#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <list>
#include <vector>
#include <algorithm>
#include <numeric>
#include <cstdint>
#include <map>
#include <set>
#include <utility>

using namespace std;

struct Point {
    int x;
    int y;
    Point(string s) {
        replace(s.begin(), s.end(), ',', ' ');
        stringstream ss(s);
        ss >> x >> y;
        cout << "New point at: " << x << ',' << y << '\n';
    }
};

struct Fold {
    int pos;
    char axis;
    Fold(string s) {
        if (s.find("fold along y=") != string::npos) {
            axis = 'y';
        } else {
            axis = 'x';
        }
        stringstream ss(s.substr(s.find('=')+1));
        ss >> pos;
        cout << "New fold on " << axis << " axis at " << pos << '\n';
    }
};

struct PaperGrid {
    int maxX;
    int maxY;
    vector<vector<char>> grid;
    PaperGrid(vector<Point> points, int maxX, int maxY) : maxX(maxX), maxY(maxY) {
        vector<char> tmpRow(maxX, '.');
        grid.resize(maxY, tmpRow);
        for (auto point: points) {
            grid[point.y][point.x] = '#';
        }
    }
    
    void fold(Fold f) {
        int delta = 0;
        int maxDelta = f.axis == 'x' ? maxX : maxY;
        int maxIdx = f.axis == 'x' ? maxY : maxX;
        while (f.pos-delta>=0 && f.pos<maxDelta) {
            for (int idx = 0; idx < maxIdx; idx++) {
                int tmpX = 0;
                int tmpX2 = 0;
                int tmpY = 0;
                int tmpY2 = 0;
                if (f.axis == 'y') {
                    tmpX = f.pos-delta;
                    tmpX2 = f.pos+delta;
                    tmpY = tmpY2 = idx;
                } else {
                    tmpX = tmpX2 = idx;
                    tmpY = f.pos - delta;
                    tmpY2 = f.pos + delta;
                }
                if (grid[tmpX2][tmpY2] == '#') {
                    grid[tmpX][tmpY] = '#'; 
                }
                grid[tmpX2][tmpY2] = ' ';
            }
            delta++;
        }

    }
    
    int countDots() {
        int result = 0;
        for (auto row: grid) {
            for (auto x: row) {
                if (x == '#') {
                    result++;
                }
            }
        }
        return result;
    }

    void print() {
        for (auto row: grid) {
            if (row[0] == ' ') {
                break;
            }
            for (auto x: row) {
                if (x == ' ') {
                    break;
                }
                cout << x << ' ';
            }
            cout << '\n';
        }
        cout << '\n';
    }
};

int main() {
    cout << "AoC2021 Day 13\n";
    ifstream infile("input");
    string line;
    vector<Point> points;
    vector<Fold> folds;
    int maxX = 0;
    int maxY = 0;
    int dotsCnt = 0;
    while(getline(infile, line)) {
        if (line.find("fold along") != string::npos) {
            Fold tmpFold(line);
            folds.push_back(tmpFold);
        } else if ( line.size() > 2) {
            Point tmpPoint(line);
            maxX = tmpPoint.x >= maxX ? tmpPoint.x+1 : maxX;
            maxY = tmpPoint.y >= maxY ? tmpPoint.y+1 : maxY;
            points.push_back(tmpPoint);
        }
    }
    cout << "Paper size: " << maxX << " x " << maxY << '\n';
    PaperGrid grid(points, maxX, maxY);
    grid.print();
    grid.fold(folds[0]);
    dotsCnt = grid.countDots();
    grid.print();
    cout << "Part 1 Result: " << dotsCnt << '\n';
    for (int idx = 1; idx < folds.size(); idx++) {
        grid.fold(folds[idx]);
    }
    grid.print();
    return 0;
}

