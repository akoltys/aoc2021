#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <list>
#include <vector>
#include <algorithm>
#include <numeric>
#include <cstdint>
#include <map>
#include <set>
#include <utility>

using namespace std;

int main() {
    cout << "AoC2021 Day 14\n";
    ifstream infile("input");
    string line;
    int64_t minMaxDiff = 0;
    // read seed
    getline(infile, line);
    string seed = line;
    // skip empty line
    getline(infile, line);
    map<string, char> rules;
    while(getline(infile, line)) {
        cout << "Parsing rule: " << line.substr(0, 2) << " => " << line.back() << '\n';
        rules[line.substr(0,2)] = line.back();
    }
    string current = seed;
    map<string, int64_t> pairs;
    for (int idx = 0; idx < seed.size()-1; idx++) {
        string tmpPair = seed.substr(idx,2);
        if (pairs.find(tmpPair) == pairs.end()) {
            pairs.insert({tmpPair, 1});
        } else {
            pairs[tmpPair] += 1;
        }
        
    }
    cout << "Processing seed: " << seed << '\n';
    for (auto p: pairs) {
        cout << "Pair: " << p.first << " cnt: " << p.second << '\n';
    }
    for (int round = 0; round < 40; round++) {
        auto tmpPairs = pairs;
        for (auto p: tmpPairs) {
            string tmpL;
            tmpL.push_back(p.first[0]);
            tmpL.push_back(rules[p.first]);
            string tmpR;
            tmpR.push_back(rules[p.first]);
            tmpR.push_back(p.first[1]);
            cout << "Pair " << p.first << " => " << tmpL << " and " << tmpR << '\n';
            if (pairs.find(tmpL) == pairs.end()) {
                pairs.insert({tmpL, p.second});
            } else {
                pairs[tmpL] += p.second;
            }

            if (pairs.find(tmpR) == pairs.end()) {
                pairs.insert({tmpR, p.second});
            } else {
                pairs[tmpR] += p.second;
            }
            pairs[p.first] -= p.second;
        }
        cout << "Pairs after round " << round+1 << '\n';
        for (auto p: pairs) {
            cout << "Pair: " << p.first << " cnt: " << p.second << '\n';
        }
       
//      not optimized version
//        string next;
//        for (int idx = 0; idx < current.size()-1; idx++) {
//            next.push_back(current[idx]);
//            string tmpPair = current.substr(idx, 2);
//            if (rules.find(tmpPair) != rules.end()) {
//                next.push_back(rules.at(tmpPair));
//            }
//        }
//        next.push_back(current.back());
//        cout << "Round " << round+1 << " : " << next << '\n';
//        current = next;
    }
    map<char, int64_t> els;
    int64_t max = 0;
    for (auto p: pairs) {
        for (auto c: p.first) {
            int64_t delta = p.second;
            if (p.first[0] != p.first[1]) {
                delta += (p.second&0x1)/2;
            }
            delta = delta / 2;
            if (els.find(c) != els.end()) {
                els[c] = els[c]+delta;
            } else {
                els[c] = delta;
            }
            if (els[c] > max) {
                max = els[c];
            }
        }
    }
    int64_t min = max;
    for (auto el: els) {
        if (el.second < min) {
            min = el.second;
        }
    }
    cout << "Min: " << min << " Max: " << max << '\n';
    minMaxDiff = max - min;
    cout << "Part 2 Result: " << minMaxDiff << '\n';
    return 0;
}

