#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <list>
#include <vector>
#include <algorithm>
#include <numeric>

using namespace std;

bool hasBingo(const vector<int>& board) {
    // horizontal
    if (board[0] == -1 && board[1] == -1 && board[2] == -1 && board[3] == -1 && board[4] == -1) {
        return true;
    }
    
    if (board[5] == -1 && board[6] == -1 && board[7] == -1 && board[8] == -1 && board[9] == -1) {
        return true;
    }
    
    if (board[10] == -1 && board[11] == -1 && board[12] == -1 && board[13] == -1 && board[14] == -1) {
        return true;
    }
    
    if (board[15] == -1 && board[16] == -1 && board[17] == -1 && board[18] == -1 && board[19] == -1) {
        return true;
    }
    
    if (board[20] == -1 && board[21] == -1 && board[22] == -1 && board[23] == -1 && board[24] == -1) {
        return true;
    }

    // vertical
    if (board[0] == -1 && board[5] == -1 && board[10] == -1 && board[15] == -1 && board[20] == -1) {
        return true;
    }

    if (board[1] == -1 && board[6] == -1 && board[11] == -1 && board[16] == -1 && board[21] == -1) {
        return true;
    }

    if (board[2] == -1 && board[7] == -1 && board[12] == -1 && board[17] == -1 && board[22] == -1) {
        return true;
    }

    if (board[3] == -1 && board[8] == -1 && board[13] == -1 && board[18] == -1 && board[23] == -1) {
        return true;
    }

    if (board[4] == -1 && board[9] == -1 && board[14] == -1 && board[19] == -1 && board[24] == -1) {
        return true;
    }

    // cross
    if (board[0] == -1 && board[6] == -1 && board[12] == -1 && board[18] == -1 && board[24] == -1) {
        return true;
    }

    if (board[4] == -1 && board[8] == -1 && board[12] == -1 && board[16] == -1 && board[20] == -1) {
        return true;
    }

    return false;
}

int main() {
    cout << "AoC2021 Day 4\n";
    ifstream infile("input");
    string line;
    vector<int> bingo_numbers;
    vector<vector<int>> boards;
    // read bingo numbers
    getline(infile, line);
    replace(line.begin(), line.end(), ',', ' ');
    cout << line << '\n';
    stringstream ss(line);
    while(!ss.eof()) {
        int tmp;
        ss >> tmp;
        bingo_numbers.push_back(tmp);
    }
    vector<int> tmp_board;
    while(getline(infile, line)) {
        if (line.size() == 0) {
            if (tmp_board.size() > 0) {
                cout << "No. " << boards.size() << " | ";
                for (auto v: tmp_board) {
                    cout << v << ' ';
                }
                cout << '\n';
                boards.push_back(tmp_board);
                tmp_board.clear();
            }
            continue;
        }
        stringstream ss(line);
        int tmp;
        while(!ss.eof()) {
            ss >> tmp;
            tmp_board.push_back(tmp);
        }
    }
    auto tmp_boards = boards;
    auto result_p1 = -1;
    for (auto number: bingo_numbers) {
        cout << "Checking number: " << number << '\n';
        for (int idx = 0; idx < tmp_boards.size(); idx++) {
            auto& board = tmp_boards[idx];
            replace(board.begin(), board.end(), number, -1);
            if (hasBingo(board)) {
                cout << "Bingo at: " << idx << '\n';
                int sumOfMarked = 0;
                for (int jdx = 0; jdx < board.size(); jdx++) {
                    if (board[jdx] != -1) {
                        sumOfMarked += boards[idx][jdx];
                    }
                }
                cout << "Number: " << number << " Sum: " << sumOfMarked << '\n';
                result_p1 = number * sumOfMarked;
                break;
            }
        }
        if (result_p1 != -1) {
            break;
        }
    }
    cout << "Part 1 Result: " << result_p1 << '\n';

    tmp_boards = boards;
    auto result_p2 = -1;
    vector<bool> bingo_reg(boards.size(), false);
    for (auto number: bingo_numbers) {
        cout << "Checking number: " << number << '\n';
        for (int idx = 0; idx < tmp_boards.size(); idx++) {
            auto& board = tmp_boards[idx];
            replace(board.begin(), board.end(), number, -1);
            if (hasBingo(board) && bingo_reg[idx] == false) {
                cout << "Bingo at: " << idx << '\n';
                int sumOfMarked = 0;
                for (int jdx = 0; jdx < board.size(); jdx++) {
                    if (board[jdx] != -1) {
                        sumOfMarked += boards[idx][jdx];
                    }
                }
                cout << "Number: " << number << " Sum: " << sumOfMarked << '\n';
                result_p2 = number * sumOfMarked;
                bingo_reg[idx] = true;
            }
        }
    }
    cout << "Part 2 Result: " << result_p2 << '\n';
    return 0;
}

