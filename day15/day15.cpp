#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <list>
#include <vector>
#include <algorithm>
#include <numeric>
#include <cstdint>
#include <map>
#include <set>
#include <utility>
#include <limits>

using namespace std;

struct XY {
    int x;
    int y;
    XY(int x, int y) : x(x), y(y) {}
};

struct XYd : public XY {
    int dist;
    XYd(int x, int y, int dist) : XY(x, y), dist(dist) {}
};

inline bool operator==(const XYd& lhs, const XYd& rhs) { 
    return lhs.x == rhs.x && lhs.y == rhs.y; 
}

inline bool operator<(const XYd& lhs, const XYd& rhs) { 
    if (lhs.dist < rhs.dist) {
        return true;
    }
    if (lhs.x != rhs.x) {
        return lhs.x < rhs.x;
    }
    return lhs.y < rhs.y;
}

void printGrid(const vector<vector<int>>& grid) {
    for (auto row: grid) {
        for (auto val: row) {
            cout << val << '\t';
        }
        cout << '\n';
    }
    cout << '\n';
}

bool isValidPos(XY pos, int maxX, int maxY) {
    if (pos.x < 0 || pos.y < 0) {
        return false;
    }
    if (pos.x >= maxX || pos.y >= maxY) {
        return false;
    }
    return true;
}

int getSafestPath(const vector<vector<int>>& grid, const XY start, const XY stop) {
    vector<vector<int>> dist = grid;
    for (auto idx = 0; idx < grid.size(); idx++) {
        for (auto jdx = 0; jdx < grid[0].size(); jdx++) {
            dist[idx][jdx] = numeric_limits<int>::max();
        }
    }
    dist[start.y][start.x] = 0;

    int maxX = grid[0].size();
    int maxY = grid.size();
    vector<XY> deltas = {XY(1,0), XY(-1,0), XY(0,1), XY(0,-1)};
    set<XYd> nxt = {{start.x, start.y, 0}};

    while(nxt.size() > 0) {
        auto next = *nxt.begin();
        nxt.erase(nxt.begin());
        for (auto d: deltas) {
            int tmpX = next.x + d.x;
            int tmpY = next.y + d.y;
            if (!isValidPos(XY(tmpX, tmpY), maxX, maxY)) {
                continue;
            }
            if (dist[tmpY][tmpX] > dist[next.y][next.x]+grid[tmpY][tmpX]) {
                if (dist[tmpY][tmpX] != numeric_limits<int>::max()) {
                    nxt.erase(XYd(tmpX, tmpY, dist[tmpY][tmpX]));
                }

                dist[tmpY][tmpX] = dist[next.y][next.x]+grid[tmpY][tmpX];
                nxt.insert(XYd(tmpX, tmpY, dist[tmpY][tmpX]));
            }
        }
    }
    return dist[stop.y][stop.x];    
}

void resizeMap(vector<vector<int>>& grid) {
    // resize rows
    int maxX = grid[0].size();
    int maxY = grid.size();
    for (int idx = 0; idx < maxY; idx++) {
        for (int jdx = maxX; jdx < 5*maxX; jdx++) {
            auto tmpVal = (grid[idx][(jdx-maxX)]+1)%10;
            if (tmpVal == 0) {
                tmpVal = 1;
            }
            grid[idx].push_back(tmpVal);
        }
    }

    // resize columns
    for (int idx = maxY; idx < 5*maxY; idx++) {
        vector<int> tmpRow;
        for (int jdx = 0; jdx < grid[0].size(); jdx++) {
            auto tmpVal = (grid[idx-maxY][jdx]+1)%10;
            if (tmpVal == 0) {
                tmpVal = 1;
            }
            tmpRow.push_back(tmpVal);
        }
        grid.push_back(tmpRow);
    }
}

int main() {
    cout << "AoC2021 Day 15\n";
    ifstream infile("input");
    string line;
    int lowestRisk = 0;
    vector<vector<int>> grid;
    while(getline(infile, line)) {
        vector<int> tmpRow;
        for (auto c: line) {
            tmpRow.push_back(c-'0');
        }
        grid.push_back(tmpRow);
    }
    printGrid(grid);
    int maxX = grid[0].size();
    int maxY = grid.size();
    lowestRisk = getSafestPath(grid, {0, 0}, {maxX-1, maxY-1});
    cout << "Part 1 Result: " << lowestRisk << '\n';
    resizeMap(grid);
    maxX = grid[0].size();
    maxY = grid.size();
    lowestRisk = getSafestPath(grid, {0, 0}, {maxX-1, maxY-1});
    cout << "Part 2 Result: " << lowestRisk << '\n';
    return 0; 
}

