#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <list>
#include <vector>

using namespace std;

int getFreqAtBit(const vector<int>& vals, int idx) {
    int mask = 1 << idx;
    int cnt = 0;
    for (auto val: vals) {
        //cout << std::hex << "Val: " << val << " mask: " << mask << " masked: " << (val&mask) << std::dec;
        if ((val&mask) != 0) {
            //cout << " INC\n";
            cnt++;
        } else {
            //cout << " NOINC\n";
        }
    }
    return cnt;
}

int main() {
    cout << "AoC2021 Day 3\n";
    ifstream infile("input");
    string line;
    unsigned int gamma = 0;
    unsigned int epsilon = 0;
    int lineCnt = 0;
    vector<int> ones = {0,0,0,0,0,0,0,0,0,0,0,0};
    vector<int> readings;
    while(getline(infile, line)) {
        lineCnt++;
        int tmp = stoi(line, 0, 2);
        int idx = 0;
        readings.push_back(tmp);
        while (tmp > 0) {
            ones[idx] += tmp&0x1;
            tmp = tmp >> 1;
            idx++;
        }
    }
    
    for (auto it = ones.rbegin(); it != ones.rend(); it++) {
        cout << *it << '\n';
        gamma = (gamma << 1) | (*it >= lineCnt/2);
    }
    epsilon = ~gamma&0xFFF;
    cout << "Part 1: gamma: " << gamma << " epsilon: " << epsilon << " power: " << gamma*epsilon << '\n';
    //readings = { 0b00100,0b11110,0b10110,0b10111,0b10101,0b01111,0b00111,0b11100,0b10000,0b11001,0b00010,0b01010};
    vector<int> tmp = readings;
    int bitIdx = 11;
    int oxygen = 0;
    int co2 = 0;
    while(bitIdx >= 0 && tmp.size() > 1) {
        auto cnt = getFreqAtBit(tmp, bitIdx);
        int bitVal = (cnt >= (tmp.size()-cnt)) << bitIdx;
        int mask = 1 << bitIdx;
        //cout << "Idx: " << bitIdx << " cnt: " << cnt << " size: " << tmp.size()<< " bitVal: " << std::hex << bitVal << " mask: " << mask << '\n';
        for (auto it = tmp.begin(); it != tmp.end(); ) {
            if ((*it & mask) != bitVal) {
                it = tmp.erase(it);
            } else {
                it++;
            }
        }
        bitIdx--;
        //cout << std::dec << "Oxygen tmp size: " << tmp.size() << " front: " << tmp[0] <<'\n';
    }
    cout << "Oxygen list size: " << tmp.size() << '\n';
    oxygen = tmp[0];
    
    bitIdx = 11;
    tmp = readings;
    while(bitIdx >= 0 && tmp.size() > 1) {
        auto cnt = getFreqAtBit(tmp, bitIdx);
        //cout << "Idx: " << bitIdx << " cnt: " << cnt << '\n';
        int bitVal = (cnt < (tmp.size()-cnt)) << bitIdx;
        int mask = 1 << bitIdx;
        for (auto it = tmp.begin(); it != tmp.end(); ) {
            if ((*it & mask) != bitVal) {
                it = tmp.erase(it);
            } else {
                it++;
            }
        }
        bitIdx--;
        //cout << "CO2 tmp size: " << tmp.size() << " front: " << tmp[0] <<'\n';
    }
    co2 = tmp[0];
    cout << "CO2 list size: " << tmp.size() << '\n';
    cout << "Part 2: oxygen: " << oxygen << " co2: " << co2 << " rating: " << oxygen*co2 << '\n';
    return 0;
}

