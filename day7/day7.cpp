#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <list>
#include <vector>
#include <algorithm>
#include <numeric>
#include <cstdint>
#include <map>

using namespace std;

int main() {
    cout << "AoC2021 Day 7\n";
    ifstream infile("input");
    string line;
    int64_t fuelCnt = -1;
    int64_t fuelCnt2 = -1;
    vector<int> crabs;
    while(getline(infile, line)) {
        replace(line.begin(), line.end(), ',', ' ');
        cout << "Input: " << line << '\n';
        stringstream ss(line);
        while(!ss.eof()) {
            int tmp;
            ss >> tmp;
            crabs.push_back(tmp);
        }
    }
    cout << "Crabs cnt: " << crabs.size() << '\n';
    sort(crabs.begin(), crabs.end());
    cout << "Min: " << crabs.front() << " Max: " << crabs.back() << '\n';
    int minCrabs = crabs.front();
    int maxCrabs = crabs.back();
    int optimalLevel = 0;
    int optimalLevel2 = 0;
    for (int level = minCrabs; level <= maxCrabs; level++) {
        int64_t tmpFuel = 0;
        int64_t tmpFuel2 = 0;
        for (auto crab: crabs) {
            int64_t tmp = abs(level-crab); 
            tmpFuel += tmp;
            tmpFuel2 += tmp*(tmp+1)/2;
        }
        
        if (fuelCnt == -1 || tmpFuel < fuelCnt) {
            fuelCnt = tmpFuel;
            optimalLevel = level;
        }

        if (fuelCnt2 == -1 ||  tmpFuel2 < fuelCnt2) {
            fuelCnt2 = tmpFuel2;
            optimalLevel2 = level;
        }
    }
    cout << "Part 1 Result: " << fuelCnt << " at level: " << optimalLevel << '\n';
    cout << "Part 2 Result: " << fuelCnt2 << " at level: " << optimalLevel2 << '\n';
    return 0;
}

