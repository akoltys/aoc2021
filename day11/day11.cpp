#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <list>
#include <vector>
#include <algorithm>
#include <numeric>
#include <cstdint>
#include <map>
#include <set>
#include <utility>

using namespace std;

using xy = pair<int, int>;

void printGrid(const vector<vector<int>>& grid) {
    for (auto row: grid) {
        for (auto val: row) {
            cout << val << ' ';
        }
        cout << '\n';
    }
    cout << "\n\n";
}

void flashAt(vector<vector<int>>& grid, int row, int col) {
    if (row-1 >= 0 && col-1 >= 0) {
        grid[row-1][col-1] += 1;
    }
    if (row-1 >= 0) {
        grid[row-1][col] += 1;
    }
    if (row-1 >= 0 && col+1 < grid[0].size()) {
        grid[row-1][col+1] += 1;
    }
    if (col-1 >= 0) {
        grid[row][col-1] += 1;
    }
    if (col+1 < grid[0].size()) {
        grid[row][col+1] += 1;
    }
    if (row+1<grid.size() && col-1>=0) {
        grid[row+1][col-1] += 1;
    }
    if (row+1<grid.size()) {
        grid[row+1][col] += 1;
    }
    if (row+1<grid.size() && col+1<grid[0].size()) {
        grid[row+1][col+1] += 1;
    }
}

int runIteration(vector<vector<int>>& grid) {
    // increase all energy levels
    for (auto r = 0; r < grid.size(); r++) {
        for (auto c = 0; c < grid[0].size(); c++) {
            grid[r][c] += 1;
        }
    }
    
    // flash all octopuses iwth energy > 9
    set<xy> flashed;
    bool noFlashes = false;
    while (!noFlashes) {
        noFlashes = true;
        for (auto r = 0; r < grid.size(); r++) {
            for (auto c = 0; c < grid[0].size(); c++) {
                if (grid[r][c] > 9) {
                    xy pt = {r, c};
                    if (flashed.find(pt) == flashed.end()) {
                        flashed.insert(pt);
                        flashAt(grid, r, c);
                        noFlashes = false;
                    }
                }
            }
        }
    }

    // set all flashed to 0
    int flashCnt = 0;
    for (auto r = 0; r < grid.size(); r++) {
        for (auto c = 0; c < grid[0].size(); c++) {
            if (grid[r][c] > 9) {
                grid[r][c] = 0;
                flashCnt += 1;
            }
        }
    }
    return flashCnt;
}

int main() {
    cout << "AoC2021 Day 9\n";
    ifstream infile("input");
    string line;
    int flashesCnt = 0;
    vector<vector<int>> octopusGrid;
    while(getline(infile, line)) {
        vector<int> tmpRow;
        cout << "Parsing: " << line << '\n';
        for (auto c: line) {
            tmpRow.push_back(c-'0');
        }
        octopusGrid.push_back(tmpRow);
    }
    printGrid(octopusGrid);
    for (int iter = 0; iter < 100; iter++) {
        flashesCnt += runIteration(octopusGrid);
    }
    printGrid(octopusGrid);
    cout << "Part 1 Result: " << flashesCnt << '\n';
    flashesCnt = 0;
    int round = 100;
    while(flashesCnt != 100) {
        flashesCnt = runIteration(octopusGrid);
        round++;
    }
    cout << "Part 2 Result: " << round << '\n';
    return 0;
}

